/*
 * CITS1002 Project 2 2012
 * Name:             Guilherme R. Lampert
 * Student number:   21203005
 * Date:             02/10/2012
 */

#ifndef MERGEDIRS_H
#define MERGEDIRS_H

/* External functions */
extern int HashFile(const char *, char[]);
extern int CopyFile(const char *, const char *);
extern int LengthOfFile(const char *, off_t *);

typedef struct DirEntry {

	char * dirName; /* Name of directory */
	size_t numFiles; /* Number of files in this dir */
	char ** filenames; /* Array with names of all files in the dir */

} DirEntry_t;

/* Command line arguments this program accepts */
typedef struct ProgramInput {

	/* Input and output directories */
	size_t numInputDirs;
	DirEntry_t * inputDirs;

	size_t outputDirStrLen;
	char * outputDir;

	/* Command line options: */

	/* -l
	 * if multiple input directories contain files of the same name,
	 * then the largest file should be copied to the output directory. */
	int l;

	/* -m
	 * if multiple input directories contain files of the same name,
	 * then the file with the most-recent modification-time should be copied to the output directory. */
	int m;

	/* -v
	 * be verbose, and report (to stdout) the full pathname of each file
	 * being copied to the output directory (otherwise, stay silent). */
	int v;

	/* -c
	 * If a conflict is found and this flag is specified,
	 * print a message and stop execution.
	 * If this flag is not specified, conflicts are printed
	 * and program execution continues. */
	int c;

	/* -i pattern
	 * if a subdirectory's name contains the indicated pattern,
	 * then it should be ignored. */
	int i;

	/* See -i flag above. NULL if flag not specified */
	char * ignorePattern;

} ProgramInput_t;

#endif // MERGEDIRS_H

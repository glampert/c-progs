/*
 * CITS2230 Operating Systems - Programming Project
 * Author:           Guilherme R. Lampert
 * Student number:   21203005
 */

#include "Scheduler.hpp"

int main(int argc, const char * argv[])
{
	try
	{
		std::auto_ptr<Scheduler> scheduler(Scheduler::Create(argc, argv));

		if (scheduler.get() != 0) // Will return null if just printing help text
		{
			scheduler->RunSimulation();
		}
	}
	catch (std::exception & e)
	{
		std::cerr << "ERROR: " << e.what() << std::endl;
	}

	return 0;
}

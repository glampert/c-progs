/*
 * CITS2230 Operating Systems - Programming Project
 * Author:           Guilherme R. Lampert
 * Student number:   21203005
 */

#ifndef PROCESS_HPP
#define PROCESS_HPP

#include <map>
#include <vector>
#include <string>

// Holds data relative to a process
class Process
{
public:

	struct TimePair
	{
		int startTime;
		int stopTime;
	};

	Process(const Process & rhs);
	Process(const std::string & name, int arrivalTime,
	        const std::vector<std::string> & codeLines);

	Process & operator = (const Process & rhs);
	bool operator < (const Process & rhs) const;

	// Data access:
	int ArrivalTime() const { return arrivalTime; }
	int & StartTime()       { return startTime;   }
	int & EndTime()         { return endTime;     }

	int NextCodeLine() const { return nextCodeLine; }
	const std::string & Name() const { return name; }

	const std::vector<std::string> & CodeLines()    const { return codeLines; }
	const std::vector<TimePair>    & SavedRRTimes() const { return RRTimes;   }

	#ifdef VERBOSE
	void Dump() const
	{
		for (size_t i = 0; i < codeLines.size(); ++i)
		{
			DLog(std::setfill('0') << std::setw(2) <<
					i+1 << " " << codeLines[i].c_str());
		}
	}
	#endif // VERBOSE

	// Add a pair of times for the RR scheduling simulation
	void AddRRTimes(int startTime, int stopTime);

	// Executes a line checking for conditional jumps (IFs)
	// and sets the 'nextCodeLine' properly.
	void ExecuteCodeLine(const std::string & codeLine);

private:

	// Default value of an "if variable" when first seen.
	static const int IfVarInitialValue;
	int * FindVar(char name);

	// Time the process arrived for execution
	int arrivalTime;

	// Time the process was first scheduled for execution
	int startTime;

	// Time when the process finished completely
	int endTime;

	std::string name;
	int nextCodeLine; // Next code line to "execute"
	std::vector<std::string> codeLines; // Lines of the process "code"

	// Pairs of start/end times for the Round Robin scheduling
	std::vector<TimePair> RRTimes;

	typedef std::map<char, int> VarsMap;
	VarsMap variables; // This value-pair set will hold the so called "if variables" found in the code.
};

#endif // PROCESS_HPP

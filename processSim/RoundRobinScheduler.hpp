/*
 * CITS2230 Operating Systems - Programming Project
 * Author:           Guilherme R. Lampert
 * Student number:   21203005
 */

#ifndef ROUND_ROBIN_SCHEDULER_HPP
#define ROUND_ROBIN_SCHEDULER_HPP

#include "Scheduler.hpp"

// Round Robin (RR) process scheduler.
class RoundRobinScheduler
	: public Scheduler
{
public:

	RoundRobinScheduler(int memDumpTimeStep,
			const std::string & procListFile,
			const std::string & outputFile,
			int timeQuantum);

	~RoundRobinScheduler();

private:

	void RunWithMemorySimulation();
	void RunWithoutMemorySimulation();

	// Time quantum of the Round Robin policy.
	int timeQuantum;
};

#endif // ROUND_ROBIN_SCHEDULER_HPP

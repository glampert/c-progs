/*
 * CITS2230 Operating Systems - Programming Project
 * Author:           Guilherme R. Lampert
 * Student number:   21203005
 */

#ifndef COMMON_HPP
#define COMMON_HPP

// Standard C/C++ libraries
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <sstream>
#include <memory> // For std::auto_ptr

// Developer log printing.
// Only prints if VERBOSE is defined.
#ifdef VERBOSE
	#define DLog(x) std::cout << x << std::endl;
#else
	#define DLog(x)
#endif // VERBOSE

// Returns the number of elements in a statically allocated array
#define ARRAY_SIZE(x) (int)(sizeof(x) / sizeof((x)[0]))

// Simple wrapper around strtol()
inline int ParseInt(const std::string & numStr, int base = 10)
{
	char * endPtr = 0;
	const char * nPtr = numStr.c_str();
	int result = static_cast<int>(strtol(nPtr, &endPtr, base));

	if (endPtr == nPtr)
	{
		// No conversion was done, result is irrelevant
		throw std::invalid_argument("Failed to parse integer from string!");
	}

	return result;
}

#endif // COMMON_HPP

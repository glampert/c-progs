/*
 * CITS2230 Operating Systems - Programming Project
 * Author:           Guilherme R. Lampert
 * Student number:   21203005
 */

#ifndef FCFS_SCHEDULER_HPP
#define FCFS_SCHEDULER_HPP

#include "Scheduler.hpp"

// First Come First Served (FCFS) process scheduler.
class FcfsScheduler
	: public Scheduler
{
public:

	FcfsScheduler(int memDumpTimeStep,
			const std::string & procListFile,
			const std::string & outputFile);

	~FcfsScheduler();

private:

	void RunWithMemorySimulation();
	void RunWithoutMemorySimulation();
};

#endif // FCFS_SCHEDULER_HPP

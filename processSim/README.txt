
================================================
CITS2230 Operating Systems - Programming Project
Author:           Guilherme R. Lampert
Student number:   21203005
================================================

Process scheduling and memory management simulation.

This is a simple process scheduling simulation for the Round Robin (RR)
and the First Come First Served (FCFS) policies, with some memory management simulation.

It is a command line application and should compile under MacOS, Linux and Windows platforms.

=========================
Building The Application:
=========================

A makefile is provided for building the command line application.
There are two targets that can be built, the "verbose" and "silent" targets.
In the "verbose" mode, several log and debug messages are printed to stdout,
this mode can be useful in identifying problems with the program.
To build the verbose mode, type "make verbose" in your terminal.
The "silent" mode does not print anything to stdout during program execution.
Errors are always printed to stderr. This mode is the default one.
To build in the silent mode, just type "make" in your terminal, or "make silent".
If the build completes successfully, a file named "processSim" will be generated,
this is the executable program.

========================
Running The Application:
========================

Run the application in your terminal by typing ./processSim [command_line_args...]
To see basic help text, run ./processSim --help

Command line examples are:
./processSim [-m mem_dump_timestep] FCFS in_file [out_file]
./processSim [-m mem_dump_timestep] RR time_quantum in_file [out_file]

If the parameter "out_file" is omitted, the output of the program will be directed
to stdout, usually being printed in the terminal.

============
Limitations:
============

The process description files can contain any number of lines,
however, the length of a line must be less than 1024 characters!

The "IF" statements in the program "code" must be strictly in one
of the following formats, respecting the given spacing between characters:

if i < 3 i=i+1 goto 4
if i < 3 i = i+1 goto 4
if i < 3 i = i + 1 goto 4

The variable of the statement (in this case "i") can be any letter of the
alphabet, but must be only 1 letter long!

The increment of the variable is fixed to 1.
The comparison operator is always expected to be '<' (less than).

If the input does not follow this assumptions, a warning for unparseable
"IFs" is generated in the "verbose" mode. In silent mode, they are quietly ignored.


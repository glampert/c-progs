
Miscellaneous C and C++ programs developed as class assignments
during my one year exchange at The University of Western Australia (UWA).

Source code is released under the [MIT License](http://opensource.org/licenses/MIT).

